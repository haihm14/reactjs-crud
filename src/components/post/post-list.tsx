import React, { useState, useEffect } from 'react';
import 'antd/dist/antd.css';
import { Table, Space, Popconfirm, notification, } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import postService from '../../service/post-service';
import { PostEdit } from '../post/post-edit';
import { Post } from '../../model/post';
import { PostAdd } from './post-add';
import notificationCommon from '../common/notification';

export function PostList() {
    const [post, setPost] = useState<Post[]>([]);
    const [api, contextHolder] = notification.useNotification();

    useEffect(() => {
        RetrievePosts();
    }, []);

    const postColumns = [
        {
            title: 'Id',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Summary',
            dataIndex: 'summary',
            key: 'summary',
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
        },
        {
            title: 'Body',
            dataIndex: 'body',
            key: 'body',
        },
        {
            title: 'Tags',
            dataIndex: 'tags',
            key: 'tags',
        },
        {
            title: 'Actions',
            key: 'actions',
            dataIndex: 'actions',
            render: (text: string, record: Post) => (
                <Space size="middle">
                    <a href={'/post-detail/' + record.id}>
                        <svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-eye" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.134 13.134 0 0 0 1.66 2.043C4.12 11.332 5.88 12.5 8 12.5c2.12 0 3.879-1.168 5.168-2.457A13.134 13.134 0 0 0 14.828 8a13.133 13.133 0 0 0-1.66-2.043C11.879 4.668 10.119 3.5 8 3.5c-2.12 0-3.879 1.168-5.168 2.457A13.133 13.133 0 0 0 1.172 8z" />
                            <path fillRule="evenodd" d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                        </svg>
                    </a>
                    {PostEdit(record)}
                    <Popconfirm title="Are you sure？" onConfirm={e => confirm(record)} icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                        <a href="#">Delete</a>
                    </Popconfirm>
                </Space>
            )
        }
    ];

    function confirm(p: Post) {
        postService.remove(p.id)
            .then(response => {
                console.log('Delete success', response.data);
                notificationCommon.openSuccessNotification(api, 'topRight', 'Delete Success');
                RetrievePosts();
            })
            .catch(e => {
                console.log(e);
            });
    }

    const RetrievePosts = () => {
        postService.getAll()
            .then(response => {
                setPost(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        <>
            {contextHolder}
            <PostAdd />
            <Table columns={postColumns} dataSource={post} rowKey={record => record.id} />
        </>
    );
}
