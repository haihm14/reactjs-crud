import React, { useState } from 'react';
import { Modal, Button, Form, InputNumber, Input } from 'antd';
import { Post } from '../../model/post';
import postService from '../../service/post-service';

export function PostAdd() {
    const [visible, SetVisible] = useState(false);

    const showModal = () => {
        SetVisible(true);
    };

    const closeModal = () => {
        SetVisible(false);
    };

    const onFinish = (values: any) => {
        const item = values['post'] as Post;
        postService.create(item)
            .then(response => {
                console.log(response.data);
                SetVisible(false);
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    };

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };

    const validateMessages = {
        // eslint-disable-next-line no-template-curly-in-string
        required: '${label} is required!',
        types: {
            // eslint-disable-next-line no-template-curly-in-string
            email: '${label} is not validate email!',
            // eslint-disable-next-line no-template-curly-in-string
            number: '${label} is not a validate number!',
        },
        number: {
            // eslint-disable-next-line no-template-curly-in-string
            range: '${label} must be between ${min} and ${max}',
        },
    };

    return (
        <div>
            <div>
                <Button type="primary" onClick={showModal}>
                    Add Post
              </Button>
            </div>
            <Modal
                title="Add post"
                visible={visible}
                okButtonProps={{ style: { display: 'none' } }}
                cancelButtonProps={{ style: { display: 'none' } }}
            >
                <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
                    <Form.Item name={['post', 'title']} label="Title" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name={['post', 'summary']} label="Summary" rules={[{ required: true }]}>
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item name={['post', 'status']} label="Status" rules={[{ type: 'number', min: 0, max: 99 }]}>
                        <InputNumber />
                    </Form.Item>
                    <Form.Item name={['post', 'body']} label="Body">
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item name={['post', 'tags']} label="Tags">
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                        <Button type="primary" htmlType="submit">Add Post</Button>
                        <Button type="default" onClick={closeModal} >Close</Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div >
    );
}