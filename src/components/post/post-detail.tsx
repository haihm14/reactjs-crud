import React, { useEffect, useState } from 'react';
import '../../index.css';
import { useParams } from 'react-router';
import postService from '../../service/post-service';
import { Post } from '../../model/post';

export function PostDetail() {
    const { id } = useParams();
    const [post, setPost] = useState<Post>();

    const getPostById = () => {
        console.log("postid", id);
        postService.get(id)
            .then(response => {
                console.log("GetById:", response.data);
                setPost(response.data);
                console.log(post);
            })
            .catch(e => {
                console.log(e);
            });
    }

    useEffect(() => {
        getPostById();
    }, []);

    return (
        <article className="article">
            <h1>{post?.title}</h1>
           <p>
               {post?.summary}
           </p>
           <hr />
           <p>
               {post?.body}
           </p>
           <hr />
           <div>
               tags: {post?.tags}
           </div>
        </article>
    );
}
