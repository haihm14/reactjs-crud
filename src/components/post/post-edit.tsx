import React, { useState } from 'react';
import { Modal, Button, Form, InputNumber, Input } from 'antd';
import { Post } from '../../model/post';
import postService from '../../service/post-service';

export function PostEdit(data: Post) {
    const [visible, SetVisible] = useState(false);

    const showModal = () => {
        SetVisible(true);
        console.log('Data:', data);
    };

    const handleOk = (e: any) => {
        console.log(e);
        SetVisible(false);
    };

    const handleCancel = (e: any) => {
        console.log(e);
        SetVisible(false);
    };

    const onFinish = (values: any) => {
        console.log('Received values of form:', values);
        const item = values['post'] as Post;
        postService.update(data.id, item)
            .then(response => {
                console.log("updated:", response.data);
                SetVisible(false);
                window.location.reload();
            })
            .catch(e => {
                console.log(e);
            });
    };

    const closeModal = () => {
        SetVisible(false);
    };

    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
    };

    const validateMessages = {
        // eslint-disable-next-line no-template-curly-in-string
        required: '${label} is required!',
        types: {
            // eslint-disable-next-line no-template-curly-in-string
            email: '${label} is not validate email!',
            // eslint-disable-next-line no-template-curly-in-string
            number: '${label} is not a validate number!',
        },
        number: {
            // eslint-disable-next-line no-template-curly-in-string
            range: '${label} must be between ${min} and ${max}',
        },
    };

    return (
        <div>
            <Button type="primary" onClick={showModal}>
                Edit
              </Button>
            <Modal
                title="Edit post"
                visible={visible}
                onOk={handleOk}
                onCancel={handleCancel}
                okButtonProps={{ style: { display: 'none' } }}
            >
                <Form {...layout} name="nest-messages" onFinish={onFinish} validateMessages={validateMessages}>
                    <Form.Item name={['post', 'title']} label="Title" rules={[{ required: true }]} initialValue={data.title}>
                        <Input />
                    </Form.Item>
                    <Form.Item name={['post', 'summary']} label="Summary" rules={[{ required: true }]} initialValue={data.summary}>
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item name={['post', 'status']} label="Status" rules={[{ type: 'number', min: 0, max: 99 }]} initialValue={data.status}>
                        <InputNumber />
                    </Form.Item>
                    <Form.Item name={['post', 'body']} label="Body" initialValue={data.body}>
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item name={['post', 'tags']} label="Tags" initialValue={data.tags}>
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                        <Button type="primary" htmlType="submit">Save</Button>
                        <Button type="default" onClick={closeModal} >Close</Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div >
    );
}