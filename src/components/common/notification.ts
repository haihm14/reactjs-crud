import { NotificationInstance } from 'antd/lib/notification';


const openSuccessNotification = (api: NotificationInstance, placement: any, message: string) => {
    api.success({
        message: message,
        placement,
    });
};

const openErrorNotification = (api: NotificationInstance, placement: any, message: string) => {
    api.error({
        message: message,
        placement,
    });
};


export default {
    openSuccessNotification,
    openErrorNotification
};