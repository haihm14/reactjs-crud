import React, { useEffect, useState } from "react";
import { CheckCircleTwoTone, FieldTimeOutlined, FundOutlined } from '@ant-design/icons';
import cardinfoService from '../../service/cardInfo-service';
import { CardInfo } from "../../model/cardInfo";

export function CardItems() {
    const [card, setCard] = useState<CardInfo>();

    useEffect(() => {
        RetrievePosts();
    }, []);

    const RetrievePosts = () => {
        cardinfoService.getAll()
            .then(response => {
                setCard(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }

    return (
        <>
            <div className="cards">
                <div className="card-item">
                    <div className="card-icon">
                        <CheckCircleTwoTone twoToneColor="#52c41a" style={{ fontSize: 32 }}></CheckCircleTwoTone>
                    </div>
                    <div className="card-info">
                        <div className='card-main-text'>{card?.resolvedTicket.toLocaleString('en')}</div>
                        {/* <div>Resloved tickets</div> */}
                    </div>
                </div>
                <div className="card-item">
                    <div className="card-icon">
                        <FieldTimeOutlined twoToneColor="#d02dee" style={{ fontSize: 32 }}></FieldTimeOutlined>
                    </div>
                    <div className="card-info">
                        <div className='card-main-text'>{card?.timeAverage.toLocaleString('en')}m</div>
                        {/* <div>Average response</div> */}
                    </div>
                </div>
                <div className="card-item">
                    <div className="card-icon">
                        <FundOutlined twoToneColor="#52c41a" style={{ fontSize: 32 }}></FundOutlined>
                    </div>
                    <div className="card-info">
                        <div className='card-main-text'>{card?.medianNPS.toLocaleString('en')}</div>
                        {/* <div>Median NPS</div> */}
                    </div>
                </div>
            </div>
        </>
    );
}