import React, { useEffect, useState } from 'react';
import { Button, Form, InputNumber, Input, DatePicker, Select, Space, notification } from 'antd';
import moment from "moment";
import { Profile } from '../../model/profile';
import profileService from '../../service/profile-service';

export function ProfileEdit() {
    const [profile, setProfile] = useState<Profile>();
    // eslint-disable-next-line no-labels

    useEffect(() => {
        profileService.getProfile()
            .then(response => {
                console.log(response.data);
                setProfile(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    }, []);

    const layout = {
        labelCol: {
            span: 4,
        },
        wrapperCol: {
            span: 14,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 4,
            span: 14,
        },
    };
    const handleSubmit = (values: any) => {
        console.log('Received values of form:', values);
        notification.open({
            message: 'Comming soon',
            description:
              'Application is using json-db server, feature will complete soon.',
            onClick: () => {
              console.log('Notification Clicked!');
            },
          });
    }

    return (
        <div className="profile-edit">
            <Form {...layout}
                initialValues={{
                    size: "large",
                }}
                layout="horizontal"
                size={"large"}
            >
                <Form.Item label="Name">
                    <Input value={profile?.name}  />
                </Form.Item>
                <Form.Item label="Title">
                    <Select value={profile?.titleValue}>
                        <Select.Option value="0">Junior</Select.Option>
                        <Select.Option value="1">Senior</Select.Option>
                        <Select.Option value="2">Manager</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item label="Role">
                    <Select value={profile?.roleValue}>
                        <Select.Option value="0">Guest</Select.Option>
                        <Select.Option value="1">Admin</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item label="Birthday">
                    <DatePicker format="DD/MM/YYYY" value={moment(profile?.birthday, 'MM-DD-YYYY')} />
                </Form.Item>

                <Form.Item label="Experience">
                    <InputNumber value={profile?.experience} />
                </Form.Item>
                <Form.Item label="Email">
                    <Input value={profile?.email} />
                </Form.Item>
                <Form.Item label="Phone">
                    <Input value={profile?.phone} />
                </Form.Item>
                <Form.Item label="Address" initialValue={profile?.address}>
                    <Input value={profile?.address} />
                </Form.Item>
                <Form.Item {...tailLayout}>
                    <Space>
                        <Button type="primary" htmlType="submit" onClick={handleSubmit}>Submit</Button>
                        <Button htmlType="button" href="/requests">Cancel</Button>
                    </Space>
                </Form.Item>
            </Form>
        </div>
    );
}
