export interface Post 
{
    id: number,
    title: string,
    author: string,
    summary: string,
    body: string,
    publishDate: Date,
    status: number,
    approver: string,
    isHot: boolean,
    tags: object
}
