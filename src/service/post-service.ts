import http from "../components/http-common"
import { Post } from '../model/post';

const getAll = () => {
  return http.get("/posts");
};

const get = (id: any) => {
  return http.get(`/posts/${id}`);
};

const create = (data: Post) => {
  return http.post("/posts", data);
};

const update = (id: any, data: any) => {
  return http.put(`/posts/${id}`, data);
};

const remove = (id: any) => {
  return http.delete(`/posts/${id}`);
};

const removeAll = () => {
  return http.delete(`/posts`);
};

const findByTitle = (title: any) => {
  return http.get(`/posts?=${title}`);
};

export default {
  getAll,
  get,
  create,
  update,
  remove,
  removeAll,
  findByTitle
};