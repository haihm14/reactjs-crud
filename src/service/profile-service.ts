import http from "../components/http-common"

const getProfile = () => {
    return http.get("/profile");
};

const updateProfile = (id: any, data: any) => {
    return http.put(`/profile/${id}`, data);
  };

export default {
    getProfile,
    updateProfile
};