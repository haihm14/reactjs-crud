import http from "../components/http-common"

const getAll = () => {
  return http.get("/cardinfo");
};

export default {
  getAll
};