import React, { } from 'react';
import 'antd/dist/antd.css';
import '../index.css';
import { Layout, Menu } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    VideoCameraOutlined,
    BarChartOutlined
} from '@ant-design/icons';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import { PostList } from '../components/post/post-list';
import { PostDetail } from '../components/post/post-detail';
import { CardItems } from '../components/cardinfo/card-list';
import { ProfileEdit } from '../components/profile/profile-edit';

const { Header, Sider, Content } = Layout;

export function LayoutDefault() {
    const state = {
        collapsed: false,
    };

    const toggle = () => {
        // setState({
        //     collapsed: !state.collapsed,
        // });
    };

    return (
        <Router>
            <Layout>
                <Sider trigger={null} collapsible collapsed={state.collapsed}>
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1" icon={<UserOutlined />}>
                            <Link to="/">Home</Link>
                        </Menu.Item>
                        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                            <Link to="/post">Posts</Link>
                        </Menu.Item>
                        <Menu.Item key="3" icon={<BarChartOutlined />}>
                            <Link to="/dashboard">Dashboard</Link>
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                        {React.createElement(state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: toggle,
                        })}
                    </Header>
                    <Content
                        className="site-layout-background"
                        style={{
                            margin: '24px 16px',
                            padding: 24,
                            minHeight: 500,
                        }}
                    >
                        <Switch>
                            <Route exact path="/">
                                <ProfileEdit />
                            </Route>
                            <Route path="/post">
                                <CardItems />
                                <hr />
                                <PostList />
                            </Route>
                            <Route path="/post-detail/:id" children={<PostDetail />}></Route>
                            <Route path="/dashboard">
                            </Route>
                        </Switch>
                    </Content>
                </Layout>
            </Layout>
        </Router>
    );
}